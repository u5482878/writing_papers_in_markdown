
A quick demo to getting papers written quickly in markdown
-----------------------------------------------------------

Packages used to build the paper:

* pandoc -- 1.19.2
* pandoc-citeproc -- 0.10.4
* pandoc-crossref -- 0.3.0.0

Feel free to ask me questions about markdown/pandoc and R integration.

